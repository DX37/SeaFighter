#ifndef MISC_HPP
#define MISC_HPP

void drawText(int x, int y, char *string);
void drawSym(int x, int y, char sym);

#endif // MISC_HPP

#include <GL/gl.h>

#include "sea.hpp"
#include "misc.hpp"

Cell::Cell(int width)
{
    sx = 0;
    sy = 0;
    fild = 0;
    kild = 0;
    w = width;
}

Grid::Grid(int width):Cell(0)
{
    w = width;
    for (int i = 0; i < 10; ++i)
        for (int j = 0; j < 10; ++j)
            cell[i][j] = new Cell(width);
}

void Cell::drawCell(int dx, int dy)
{
    sx = dx; sy = dy;
    glColor3d(1, 1, 1);
    glBegin(GL_LINES);
        glVertex2i(sx, sy);
        glVertex2i(sx + w, sy);
        glVertex2i(sx, sy);
        glVertex2i(sx, sy + w);
        glVertex2i(sx + w, sy);
        glVertex2i(sx + w, sy + w);
        glVertex2i(sx, sy + w);
        glVertex2i(sx + w, sy + w);
    glEnd();
}

void Cell::fillCell(double r, double g, double b, double a)
{
    glColor4d(r, g, b, a);
    glBegin(GL_QUADS);
        glVertex2i(sx, sy);
        glVertex2i(sx + w, sy);
        glVertex2i(sx + w, sy + w);
        glVertex2i(sx, sy + w);
    glEnd();

    glColor4d(1, 1, 1, 1);
    glBegin(GL_LINES);
        glVertex2i(sx, sy);
        glVertex2i(sx + w, sy);
        glVertex2i(sx, sy);
        glVertex2i(sx, sy + w);
        glVertex2i(sx + w, sy);
        glVertex2i(sx + w, sy + w);
        glVertex2i(sx, sy + w);
        glVertex2i(sx + w, sy + w);
    glEnd();
}

void Cell::killCell()
{
    if (fild <= 0)
    {
        glPointSize(6);
        glColor3d(1, 1, 0);
        glBegin(GL_POINTS);
            glVertex2i(sx + w/2, sy + w/2);
        glEnd();
        glPointSize(1);
        glColor3d(1, 1, 1);
    }
    else
    {
        glLineWidth(2);
        glColor3d(1, 1, 0);
        glBegin(GL_LINES);
            glVertex2i(sx, sy);
            glVertex2i(sx + w, sy + w);
            glVertex2i(sx + w, sy);
            glVertex2i(sx, sy + w);
        glEnd();
        glColor3d(1, 1, 1);
        glLineWidth(1);
    }
}

void Grid::drawGrid(int kx, int ky, int hide, double r, double g, double b, double a)
{
    int x = kx, y = ky;

    for (int i = 0; i < 10; ++i)
    {
        drawSym(x+10, y-10, i+'a');
        for (int j = 0; j < 10; ++j)
        {
            if (i == 0)
                drawSym(x-20, y+20, j+'0');

            cell[i][j]->drawCell(x, y);

            if (this->getFill(i, j) <= 0 && this->getKill(i, j) == 1)
                cell[i][j]->killCell();

            if (!hide)
            {
                if (this->getKill(i, j) == 0 && this->getFill(i, j) >= 1)
                    cell[i][j]->fillCell(r, g, b, a);

                if (this->getKill(i, j) == 1 && this->getFill(i, j) >= 1)
                {
                    cell[i][j]->fillCell(r, g, b, a);
                    cell[i][j]->killCell();
                }
            }
            else
            {
                if (this->getKill(i, j) == 1 && this->getFill(i, j) == 2)
                    cell[i][j]->killCell();

                if (this->getKill(i, j) == 1 && this->getFill(i, j) == 3)
                {
                    cell[i][j]->fillCell(r, g, b, a);
                    cell[i][j]->killCell();
                }
            }


            y += w;
        }
        x += w;
        y = ky;
    }
}

int Cell::cell_getFill(Cell *cell)
{
    return cell->fild;
}

int Cell::cell_getKill(Cell *cell)
{
    return cell->kild;
}

void Cell::cell_setFill(Cell *cell, int f)
{
    cell->fild = f;
}

void Cell::cell_setKill(Cell *cell, int k)
{
    cell->kild = k;
}

int Cell::cell_getX(Cell *cell)
{
    return cell->sx;
}

int Cell::cell_getY(Cell *cell)
{
    return cell->sy;
}

int Cell::cell_getW(Cell *cell)
{
    return cell->w;
}

int Grid::getFill(int a, int b)
{
    return cell_getFill(cell[a][b]);
}

int Grid::getKill(int a, int b)
{
    return cell_getKill(cell[a][b]);
}

void Grid::setFill(int a, int b, int f)
{
    cell_setFill(cell[a][b], f);
}

void Grid::setKill(int a, int b, int k)
{
    cell_setKill(cell[a][b], k);
}

int Grid::matchCoord(int ax, int ay, int a, int b)
{
    return (ax == this->cell_getX(cell[a][b]) && ay == this->cell_getY(cell[a][b]));
}

int Grid::existInCoord(int ax, int ay, int a, int b)
{
    return (ax >= this->cell_getX(cell[a][b]) && ax <= this->cell_getX(cell[a][b]) + this->cell_getW(cell[a][b])
            && ay >= this->cell_getY(cell[a][b]) && ay <= this->cell_getY(cell[a][b]) + this->cell_getW(cell[a][b]));
}

#include "sea.hpp"
#include "fighter.hpp"

Ship::Ship()
{
    cell = 0;
    x = 0; y = 0; s = 0; w = 0;
}

F1::F1(int width)
{
    s = 1;
    w = width;
    cell = new Cell*[s];
    for (int i = 0; i < s; ++i)
        cell[i] = new Cell(w);
}

F2::F2(int width)
{
    s = 2;
    w = width;
    cell = new Cell*[s];
    for (int i = 0; i < s; ++i)
        cell[i] = new Cell(w);
}

F3::F3(int width)
{
    s = 3;
    w = width;
    cell = new Cell*[s];
    for (int i = 0; i < s; ++i)
        cell[i] = new Cell(w);
}

F4::F4(int width)
{
    s = 4;
    w = width;
    cell = new Cell*[s];
    for (int i = 0; i < s; ++i)
        cell[i] = new Cell(w);
}

void Ship::drawShip(int kx, int ky, int mode)
{
    x = kx; y = ky;
    for (int i = 0; i < s; ++i)
    {
        if (mode == 1)
            cell[i]->drawCell(x + (w * i), y);
        else if (mode == -1)
            cell[i]->drawCell(x, y + (w * i));
        cell[i]->fillCell(1, 1, 1, 1);
    }
}

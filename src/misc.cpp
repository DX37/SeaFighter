#include <string.h>
#include <GL/freeglut.h>

void drawText(int x, int y, char *string)
{
    glRasterPos2f(x, y);
    int len, i;
    len = (int)strlen(string);
    for (i = 0; i < len; i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, string[i]);
}

void drawSym(int x, int y, char sym)
{
    glRasterPos2f(x, y);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, sym);
}

#ifndef SEA_HPP
#define SEA_HPP

class Cell
{
    private:
        int sx, sy, w, fild, kild;
    public:
        Cell(int);
        void drawCell(int, int);
        void fillCell(double, double, double, double);
        void killCell();
        int cell_getFill(Cell*);
        int cell_getKill(Cell*);
        void cell_setFill(Cell*, int);
        void cell_setKill(Cell*, int);
        int cell_getX(Cell*);
        int cell_getY(Cell*);
        int cell_getW(Cell*);
};

class Grid: public Cell
{
    public:
        int w;
        Cell *cell[10][10];
        Grid(int);
        void drawGrid(int, int, int, double, double, double, double);
        int getFill(int, int);
        int getKill(int, int);
        void setFill(int, int, int);
        void setKill(int, int, int);
        int matchCoord(int, int, int, int);
        int existInCoord(int, int, int, int);
};

#endif // SEA_HPP

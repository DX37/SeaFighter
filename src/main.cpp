#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <GL/freeglut.h>
#include <GLFW/glfw3.h>

#include "sea.hpp"
#include "fighter.hpp"
#include "misc.hpp"

#define player 0
#define enemy 1

#define false 0
#define true 1
#define HIDE_ENEMY_GRID true

const int grid_x = 40, grid_y = 40;

int grid_width = 30;
int gx = grid_x, gy = grid_y, mode = 1, game_state = 0, siz = 0, st = 0, player_cells = 20, enemy_cells = 20;
int ships = 10, turn = player;
char win_message[10], press_enter[30];

Grid *player_grid;
Grid *enemy_grid;

void drawAr(int s, Grid *grid)
{
    Cell *tmp = new Cell(grid_width);

    if (mode == 1)
    {
        for (int i = gx; i < gx + grid_width*s; i += grid_width)
        {
            if (gy - grid_width >= grid_y)
            {
                tmp->drawCell(i, gy - grid_width);
                tmp->fillCell(0.5, 0.5, 0.5, 0.75);
            }
        }
        for (int i = gx; i < gx + grid_width*s; i += grid_width)
        {
            if (gy + grid_width <= grid_y + grid_width*9)
            {
                tmp->drawCell(i, gy + grid_width);
                tmp->fillCell(0.5, 0.5, 0.5, 0.75);
            }
        }

        if (gx - grid_width >= grid_x)
        {
            tmp->drawCell(gx - grid_width, gy);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx + grid_width <= grid_x + grid_width*10 - grid_width*s)
        {
            tmp->drawCell(gx + grid_width*s, gy);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }

        if (gx - grid_width >= grid_x && gy - grid_width >= grid_y)
        {
            tmp->drawCell(gx - grid_width, gy - grid_width);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx - grid_width >= grid_x && gy + grid_width <= grid_y + grid_width*9)
        {
            tmp->drawCell(gx - grid_width, gy + grid_width);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx + grid_width*s < grid_x + grid_width*10 && gy - grid_width >= grid_y)
        {
            tmp->drawCell(gx + grid_width*s, gy - grid_width);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx + grid_width*s < grid_x + grid_width*10 && gy + grid_width < grid_y + grid_width*10)
        {
            tmp->drawCell(gx + grid_width*s, gy + grid_width);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
    }
    else if (mode == -1)
    {
        for (int i = gy; i < gy + grid_width*s; i += grid_width)
        {
            if (gx - grid_width >= grid_x)
            {
                tmp->drawCell(gx - grid_width, i);
                tmp->fillCell(0.5, 0.5, 0.5, 0.75);
            }
        }
        for (int i = gy; i < gy + grid_width*s; i += grid_width)
        {
            if (gx + grid_width <= grid_x + grid_width*9)
            {
                tmp->drawCell(gx + grid_width, i);
                tmp->fillCell(0.5, 0.5, 0.5, 0.75);
            }
        }

        if (gy - grid_width >= grid_y)
        {
            tmp->drawCell(gx, gy - grid_width);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gy + grid_width <= grid_y + grid_width*10 - grid_width*s)
        {
            tmp->drawCell(gx, gy + grid_width*s);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx - grid_width >= grid_x && gy - grid_width >= grid_y)
        {
            tmp->drawCell(gx - grid_width, gy - grid_width);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx < grid_x + grid_width*9 && gy - grid_width >= grid_y)
        {
            tmp->drawCell(gx + grid_width, gy - grid_width);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx - grid_width >= grid_x && gy + grid_width*s < grid_y + grid_width*10)
        {
            tmp->drawCell(gx - grid_width, gy + grid_width*s);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
        if (gx + grid_width < grid_x + grid_width*10 && gy + grid_width*s < grid_y + grid_width*10)
        {
            tmp->drawCell(gx + grid_width, gy + grid_width*s);
            tmp->fillCell(0.5, 0.5, 0.5, 0.75);
        }
    }
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (mode == 1)
    {
        if (key == GLFW_KEY_LEFT && action == GLFW_PRESS && gx > grid_x)
            gx -= grid_width;
        if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS && gx < grid_x + grid_width*10 - grid_width*siz)
            gx += grid_width;
        if (key == GLFW_KEY_UP && action == GLFW_PRESS && gy > grid_y)
            gy -= grid_width;
        if (key == GLFW_KEY_DOWN && action == GLFW_PRESS && gy < grid_y + grid_width*9)
            gy += grid_width;
    }
    else
    {
        if (key == GLFW_KEY_LEFT && action == GLFW_PRESS && gx > grid_x)
            gx -= grid_width;
        if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS && gx < grid_x + grid_width*9)
            gx += grid_width;
        if (key == GLFW_KEY_UP && action == GLFW_PRESS && gy > grid_y)
            gy -= grid_width;
        if (key == GLFW_KEY_DOWN && action == GLFW_PRESS && gy < grid_y + grid_width*10 - grid_width*siz)
            gy += grid_width;
    }

    if (key == GLFW_KEY_TAB && action == GLFW_PRESS)
        mode *= -1;

    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {
        for (int i = 0; i < 10; ++i)
            for (int j = 0; j < 10; ++j)
                if (player_grid->matchCoord(gx, gy, i, j) && game_state < 10)
                {
                    for (int s = 0; s < siz; ++s)
                    {
                        if (mode == 1)
                        {
                            player_grid->setFill(i+s, j, 1);

                            if (j-1 >= 0 && j <= 9)
                                player_grid->setFill(i+s, j-1, -1);

                            if (j >= 0 && j+1 <= 9)
                                player_grid->setFill(i+s, j+1, -1);

                            if (s == 0 && i-1 >= 0 && j-1 >= 0)
                                player_grid->setFill(i-1, j-1, -1);

                            if (s == 0 && j+1 <= 9 && i-1 >= 0)
                                player_grid->setFill(i-1, j+1, -1);

                            if (s == siz-1 && j-1 >= 0 && i+siz <= 9)
                                player_grid->setFill(i+siz, j-1, -1);

                            if (s == siz-1 && j+1 <= 9 && i+siz <= 9)
                                player_grid->setFill(i+siz, j+1, -1);

                            if (s == 0 && i-1 >= 0)
                                player_grid->setFill(i-1, j, -1);

                            if (s == siz-1 && i+siz <= 9)
                                player_grid->setFill(i+siz, j, -1);
                        }
                        else
                        {
                            player_grid->setFill(i, j+s, 1);

                            if (i-1 >= 0 && i <= 9)
                                player_grid->setFill(i-1, j+s, -1);

                            if (i >= 0 && i+1 <= 9)
                                player_grid->setFill(i+1, j+s, -1);

                            if (s == 0 && i-1 >= 0 && j-1 >= 0)
                                player_grid->setFill(i-1, j-1, -1);

                            if (s == 0 && i+1 <= 9 && j-1 >= 0)
                                player_grid->setFill(i+1, j-1, -1);

                            if (s == siz-1 && i-1 >= 0 && j+siz <= 9)
                                player_grid->setFill(i-1, j+siz, -1);

                            if (s == siz-1 && i+1 <= 9 && j+siz <= 9)
                                player_grid->setFill(i+1, j+siz, -1);

                            if (s == 0 && j-1 >= 0)
                                player_grid->setFill(i, j-1, -1);

                            if (s == siz-1 && j+siz <= 9)
                                player_grid->setFill(i, j+siz, -1);
                        }
                    }
                    gx = grid_x; gy = grid_y; mode = 1; st = 0;
                    ++game_state;
                    ++st;
                }
    }
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && game_state < 11)
    {
        for (int i = 0; i < 10; ++i)
            for (int j = 0; j < 10; ++j)
            {
                int dst = 0;
                if (enemy_grid->existInCoord(xpos, ypos, i, j))
                {
                    if (enemy_grid->getFill(i, j) <= 0 && enemy_grid->getKill(i, j) == 0)
                    {
                        enemy_grid->setKill(i, j, 1);
                        turn = enemy;
                    }
                    else if (enemy_grid->getFill(i, j) >= 1 && enemy_grid->getKill(i, j) == 0)
                    {
                        enemy_grid->setKill(i, j, 1);
                        enemy_grid->setFill(i, j, 2);

                        if (i-1 >= 0 && enemy_grid->getFill(i-1, j) >= 1 && enemy_grid->getKill(i-1, j) == 0)
                            dst = 0;
                        else if (i+1 <= 9 && enemy_grid->getFill(i+1, j) >= 1 && enemy_grid->getKill(i+1, j) == 0)
                            dst = 0;
                        else if (j-1 >= 0 && enemy_grid->getFill(i, j-1) >= 1 && enemy_grid->getKill(i, j-1) == 0)
                            dst = 0;
                        else if (j+1 <= 9 && enemy_grid->getFill(i, j+1) >= 1 && enemy_grid->getKill(i, j+1) == 0)
                            dst = 0;
                        else
                            dst = 1;

                        if (i-1 >= 0 && enemy_grid->getFill(i-1, j) == -1 && enemy_grid->getKill(i-1, j) == 0)
                            enemy_grid->setFill(i-1, j, -2);

                        if (i+1 <= 9 && enemy_grid->getFill(i+1, j) == -1 && enemy_grid->getKill(i+1, j) == 0)
                            enemy_grid->setFill(i+1, j, -2);

                        if (j-1 >= 0 && enemy_grid->getFill(i, j-1) == -1 && enemy_grid->getKill(i, j-1) == 0)
                            enemy_grid->setFill(i, j-1, -2);

                        if (j+1 <= 9 && enemy_grid->getFill(i, j+1) == -1 && enemy_grid->getKill(i, j+1) == 0)
                            enemy_grid->setFill(i, j+1, -2);

                        //

                        if (i-1 >= 0 && j-1 >= 0 && enemy_grid->getFill(i-1, j-1) == -1 && enemy_grid->getKill(i-1, j-1) == 0)
                            enemy_grid->setFill(i-1, j-1, -2);

                        if (i+1 <= 9 && j-1 >= 0 && enemy_grid->getFill(i+1, j-1) == -1 && enemy_grid->getKill(i+1, j-1) == 0)
                            enemy_grid->setFill(i+1, j-1, -2);

                        if (i-1 >=0 && j+1 <= 9 && enemy_grid->getFill(i-1, j+1) == -1 && enemy_grid->getKill(i-1, j+1) == 0)
                            enemy_grid->setFill(i-1, j+1, -2);

                        if (i+1 <= 9 && j+1 <= 9 && enemy_grid->getFill(i+1, j+1) == -1 && enemy_grid->getKill(i+1, j+1) == 0)
                            enemy_grid->setFill(i+1, j+1, -2);

                        --enemy_cells;
                    }
                }
                if (dst)
                {
                    for (int a = 0; a < 10; ++a)
                        for (int b = 0; b < 10; ++b)
                        {
                            if (enemy_grid->getFill(a, b) == -2)
                                enemy_grid->setKill(a, b, 1);
                            if (enemy_grid->getFill(a, b) == 2)
                                enemy_grid->setFill(a, b,  3);
                        }
                    dst = 0;
                }
                if (enemy_cells == 0)
                {
                    ++game_state;
                    sprintf(win_message, "You win!");
                }
                else if (player_cells == 0)
                {
                    ++game_state;
                    sprintf(win_message, "You lose!");
                }
            }
    }
}

int main(int argc, char **argv)
{
    srand(time(NULL));
    sprintf(press_enter, "Press ENTER to exit the game");
    int width = grid_x + grid_width*24, height = grid_y + grid_width*13, copd = 0, m, n, dst;
    GLFWwindow* window;
    F4 *f4[1]; F3 *f3[2]; F2 *f2[3]; F1 *f1[4];

    glutInit(&argc, argv);

    if (!glfwInit())
        return -1;

    player_grid = new Grid(grid_width);
    enemy_grid = new Grid(grid_width);

    for (int i = 0; i < 1; ++i)
        f4[i] = new F4(grid_width);

    for (int i = 0; i < 2; ++i)
        f3[i] = new F3(grid_width);

    for (int i = 0; i < 3; ++i)
        f2[i] = new F2(grid_width);

    for (int i = 0; i < 4; ++i)
        f1[i] = new F1(grid_width);

    Grid *tmp_grid = new Grid(grid_width);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    window = glfwCreateWindow(width, height, "Sea Fighter", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    while (!glfwWindowShouldClose(window))
    {
        glfwGetFramebufferSize(window, &width, &height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glViewport(0, 0, width, height);
        glLoadIdentity();
        glOrtho(0, width, height, 0, 1, -1);

        player_grid->drawGrid(grid_x, grid_y, false, 0, 0, 1, 1);

        if (game_state == 0)
        {
            siz = 4;
            f4[st]->drawShip(gx, gy, mode);
            drawAr(siz, tmp_grid);
        }
        else if (game_state >= 1 && game_state <= 2)
        {
            siz = 3;
            f3[st]->drawShip(gx, gy, mode);
            drawAr(siz, tmp_grid);
        }
        else if (game_state >= 3 && game_state <= 5)
        {
            siz = 2;
            f2[st]->drawShip(gx, gy, mode);
            drawAr(siz, tmp_grid);
        }
        else if (game_state >= 6 && game_state <= 9)
        {
            siz = 1;
            f1[st]->drawShip(gx, gy, mode);
            drawAr(siz, tmp_grid);
        }

        if (game_state == 10 && !copd)
        {
            for (int i = 0; i < 10; ++i)
                for (int j = 0; j < 10; ++j)
                    enemy_grid->setFill(j, i, player_grid->getFill(i, j));

            Cell *tmp = new Cell(grid_width);
            for (int i = 0; i < 10/2; ++i)
                for (int j = 0; j < 10; ++j)
                {
                    tmp = enemy_grid->cell[9-i][j];
                    enemy_grid->cell[9-i][j] = enemy_grid->cell[i][j];
                    enemy_grid->cell[i][j] = tmp;
                }
            copd = 1;
        }

        if (game_state >= 10)
        {
            dst = 0;
            enemy_grid->drawGrid(grid_width*10 + 100, grid_y, HIDE_ENEMY_GRID, 1, 0, 0, 1);
        }

        if (game_state > 10)
        {
            drawText(grid_x + grid_width*10 - grid_width/2, grid_y + grid_width*11, win_message);
            drawText(grid_x + grid_width*6 + grid_width/2, grid_y + grid_width*12, press_enter);
            if (glfwGetKey(window, GLFW_KEY_ENTER) == GLFW_PRESS)
                glfwSetWindowShouldClose(window, GLFW_TRUE);
        }

        while (turn == enemy)
        {
            m = rand() % 10;
            n = rand() % 10;
            if (player_grid->getFill(m, n) <= 0 && player_grid->getKill(m, n) == 0)
            {
                player_grid->setKill(m, n, 1);
                turn = player;
            }
            else if (player_grid->getFill(m, n) >= 1 && player_grid->getKill(m, n) == 0)
            {
                player_grid->setKill(m, n, 1);

                if (m-1 >= 0 && player_grid->getFill(m-1, n) >= 1 && player_grid->getKill(m-1, n) == 0)
                    dst = 0;
                else if (m+1 <= 9 && player_grid->getFill(m+1, n) >= 1 && player_grid->getKill(m+1, n) == 0)
                    dst = 0;
                else if (n-1 >= 0 && player_grid->getFill(m, n-1) >= 1 && player_grid->getKill(m, n-1) == 0)
                    dst = 0;
                else if (n+1 <= 9 && player_grid->getFill(m, n+1) >= 1 && player_grid->getKill(m, n+1) == 0)
                    dst = 0;
                else
                    dst = 1;

                if (m-1 >= 0 && player_grid->getFill(m-1, n) == -1 && player_grid->getKill(m-1, n) == 0)
                    player_grid->setFill(m-1, n, -2);

                if (m+1 <= 9 && player_grid->getFill(m+1, n) == -1 && player_grid->getKill(m+1, n) == 0)
                    player_grid->setFill(m+1, n, -2);

                if (n-1 >= 0 && player_grid->getFill(m, n-1) == -1 && player_grid->getKill(m, n-1) == 0)
                    player_grid->setFill(m, n-1, -2);

                if (n+1 <= 9 && player_grid->getFill(m, n+1) == -1 && player_grid->getKill(m, n+1) == 0)
                    player_grid->setFill(m, n+1, -2);

                //

                if (m-1 >= 0 && n-1 >= 0 && player_grid->getFill(m-1, n-1) == -1 && player_grid->getKill(m-1, n-1) == 0)
                    player_grid->setFill(m-1, n-1, -2);

                if (m+1 <= 9 && n-1 >= 0 && player_grid->getFill(m+1, n-1) == -1 && player_grid->getKill(m+1, n-1) == 0)
                    player_grid->setFill(m+1, n-1, -2);

                if (m-1 >=0 && n+1 <= 9 && player_grid->getFill(m-1, n+1) == -1 && player_grid->getKill(m-1, n+1) == 0)
                    player_grid->setFill(m-1, n+1, -2);

                if (m+1 <= 9 && n+1 <= 9 && player_grid->getFill(m+1, n+1) == -1 && player_grid->getKill(m+1, n+1) == 0)
                    player_grid->setFill(m+1, n+1, -2);

                --player_cells;
            }
            if (dst)
            {
                for (int a = 0; a < 10; ++a)
                    for (int b = 0; b < 10; ++b)
                        if (player_grid->getFill(a, b) == -2)
                            player_grid->setKill(a, b, 1);
                dst = 0;
            }
            if (enemy_cells == 0)
            {
                ++game_state;
                sprintf(win_message, "You win!");
            }
            else if (player_cells == 0)
            {
                ++game_state;
                sprintf(win_message, "You lose!");
            }
        }

        glfwSwapInterval(1);
        glfwSwapBuffers(window);

        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

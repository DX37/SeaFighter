#include "sea.hpp"

#ifndef FIGHTER_HPP
#define FIGHTER_HPP

class Ship
{
    public:
        int x, y, s, w;
        Ship();
        Cell **cell;
        virtual void drawShip(int, int, int);
};

class F1: public Ship
{
    public:
        F1(int);
};

class F2: public Ship
{
    public:
        F2(int);
};

class F3: public Ship
{
    public:
        F3(int);
};

class F4: public Ship
{
    public:
        F4(int);
};

#endif // FIGHTER_HPP

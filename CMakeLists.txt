cmake_minimum_required(VERSION 3.0)

project(SeaFighter)

find_package(PkgConfig REQUIRED)

pkg_search_module(GL REQUIRED gl)
if (NOT GL_FOUND)
    message(SEND_ERROR "Failed to find gl")
    return()
else()
    include_directories(${GL_INCLUDE_DIRS})
endif()

pkg_search_module(FREEGLUT REQUIRED freeglut)
if (NOT FREEGLUT_FOUND)
    message(SEND_ERROR "Failed to find freeglut")
    return()
else()
    include_directories(${FREEGLUT_INCLUDE_DIRS})
endif()

pkg_search_module(GLFW3 REQUIRED glfw3)
if (NOT GLFW3_FOUND)
    message(SEND_ERROR "Failed to find glfw3")
    return()
else()
    include_directories(${GLFW3_INCLUDE_DIRS})
endif()

set(SOURCE src/main.cpp src/fighter.cpp src/sea.cpp src/misc.cpp)
include_directories(src)

add_executable(SeaFighter ${SOURCE})

target_compile_options(SeaFighter PUBLIC ${FREEGLUT_CFLAGS} ${GLFW3_CFLAGS} ${GL_CFLAGS})
target_link_libraries(SeaFighter ${FREEGLUT_LIBRARIES} ${GLFW3_LIBRARIES} ${GL_LIBRARIES})
